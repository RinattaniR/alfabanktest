package report;

import driver.DriverProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.File;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class TestReport{

    private static Logger logger = LoggerFactory.getLogger(TestReport.class);
    FileWriter outputStream = null;
    BufferedWriter bufferedWriter =null;

    private String timeOfReport(){
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy_MM_dd_HH_mm_ss");
        LocalDateTime now = LocalDateTime.now();
        return dtf.format(now);
    }

    private File createFile() throws IOException {
        String filepath = System.getProperty("filepath");
        logger.debug("filepath = " + filepath);
        File myFile = new File( filepath + "/" + System.getProperty("browserName") + "_" + timeOfReport()+".txt");
        myFile.createNewFile();
        return myFile;
    }

    public void generate(String infoTxt) throws IOException {

        try {
            bufferedWriter = new BufferedWriter(outputStream = new FileWriter(createFile(),false));
            bufferedWriter.write(infoTxt);
            bufferedWriter.flush();


        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.close();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
            if (bufferedWriter != null) {
                try {
                    bufferedWriter.close();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        }
    }
}

