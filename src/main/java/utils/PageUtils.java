package utils;

import driver.DriverProvider;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import static configuration.GlobalTimeConstants.*;

public class PageUtils {
    private static Logger logger = LoggerFactory.getLogger(PageUtils.class);
    private static final String JS_SCRIPT_EXEC = "return document.readyState;";
    private static final String JS_SCRIPT_FOR_SCROLL_INTO = "arguments[0].scrollIntoView(true);";
    private static final String JS_SCRIPT_GET_INNER_TEXT= "return arguments[0].innerText";

    public static void waitForPageLoading() {
        new WebDriverWait(DriverProvider.getDriver(), TIMEOUT_SEC).until((ExpectedCondition<Boolean>) driver1 -> {
            JavascriptExecutor js = (JavascriptExecutor) driver1;
            return (Boolean) js.executeScript("return jQuery.active == 0");
        });
    }

    public static void clickIfEnabled(WebElement element) {
        logger.debug("clickIfEnabled.before wait = " + System.currentTimeMillis());
        fluentWait(element);
        logger.debug("clickIfEnabled.after wait = " + System.currentTimeMillis());
        element.click();
    }

    public static void waitForDomIsReady() {
        WebDriverWait wait = new WebDriverWait(DriverProvider.getDriver(), LONG_WAIT_SEC);
        wait.withTimeout(LONG_WAIT_SEC, TimeUnit.SECONDS).
                pollingEvery(POLLING_WAIT_MILISEC, TimeUnit.MILLISECONDS).
                ignoring(WebDriverException.class).
                until(Boolean -> ((JavascriptExecutor) Boolean).executeScript(JS_SCRIPT_EXEC).equals("complete"));
    }

    public static void fluentWait(WebElement element) {

        new FluentWait<WebElement>(element).
                withTimeout(LONG_WAIT_SEC, TimeUnit.SECONDS).
                pollingEvery(POLLING_WAIT_MILISEC, TimeUnit.MILLISECONDS).
                ignoring(WebDriverException.class).
                until((new Function<WebElement, Boolean>() {
                    @Override
                    public Boolean apply(WebElement element) {
                        try {
                            if (element.getSize().getHeight() != 0 && element.getSize().getWidth() != 0 && element.isDisplayed()
                                    && element.isEnabled()) {
                                return true;
                            } else {
                                return false;
                            }
                        } catch (NoSuchElementException nse) {
                            logger.info("fluentWaitForElementToBeInvisible: element is not attached due to " + nse);
                            return false;
                        } catch (StaleElementReferenceException ser) {
                            logger.info("fluentWaitForElementToBeInvisible: element is not attached due to " + ser);
                            return false;
                        }
                    }
                }));
    }

    public static void scrollToElement(WebElement element) {
        ((JavascriptExecutor) DriverProvider.getDriver()).executeScript(JS_SCRIPT_FOR_SCROLL_INTO, element);
    }

    public static String getInnerText(WebElement element){
        return (String)((JavascriptExecutor) DriverProvider.getDriver())
                .executeScript(JS_SCRIPT_GET_INNER_TEXT, element);
    }

    public static void setValueForInputFields(WebElement element,String value){
        if(value!=null) {
            clickIfEnabled(element);
            element.sendKeys(value);
        }
    }

    public static void switchToNewWindow(){
        WebDriver driver = DriverProvider.getDriver();
        driver.close();
        ArrayList<String> tabs = new ArrayList(driver.getWindowHandles());
        logger.info("Tabs count = " + tabs.size());
        driver.switchTo().window(tabs.get(0));
    }
}
