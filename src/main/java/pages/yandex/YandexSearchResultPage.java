package pages.yandex;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import pages.common.AbstractPage;
import utils.PageUtils;

public class YandexSearchResultPage  extends AbstractPage {

    private WebElement alfaBankLnk(){
        return getDriver().findElement(By.xpath("//a[@href='https://AlfaBank.ru/']"));
    }

    public void followAlfaBankLink(){
        PageUtils.waitForPageLoading();
        PageUtils.waitForDomIsReady();
        PageUtils.clickIfEnabled(alfaBankLnk());
    }

}
