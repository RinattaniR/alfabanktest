package pages.yandex;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import pages.common.AbstractPage;
import utils.PageUtils;

public class YandexStartPage extends AbstractPage {

    private WebElement searchTxt(){
        return getDriver().findElement(By.id("text"));
    }

    private WebElement searchBtn(){
        return getDriver().findElement(By.xpath("//button[@type='submit']"));
    }

    public void typeInSearchBox(String searchString){
          PageUtils.waitForPageLoading();
          PageUtils.setValueForInputFields(searchTxt(),searchString);
          PageUtils.clickIfEnabled(searchBtn());
    }
}
