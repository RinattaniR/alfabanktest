package pages;

import driver.DriverProvider;
import org.openqa.selenium.support.PageFactory;
import pages.alphaBank.AlfaBankAboutPage;
import pages.alphaBank.AlfaBankStartPage;
import pages.alphaBank.AlfaBankVacanciesPage;
import pages.yandex.YandexSearchResultPage;
import pages.yandex.YandexStartPage;

public class Pages {

    public static YandexStartPage yandexStartPage() {
            return PageFactory.initElements(DriverProvider.getDriver(), YandexStartPage.class);
    }

    public static YandexSearchResultPage yandexSearchResultPage() {
        return PageFactory.initElements(DriverProvider.getDriver(), YandexSearchResultPage.class);
    }

    public static AlfaBankStartPage alphaBankStartPage() {
        return PageFactory.initElements(DriverProvider.getDriver(), AlfaBankStartPage.class);
    }

    public static AlfaBankAboutPage alphaBankAboutPage() {
        return PageFactory.initElements(DriverProvider.getDriver(), AlfaBankAboutPage.class);
    }

    public static AlfaBankVacanciesPage alphaBankVacanciesPage() {
        return PageFactory.initElements(DriverProvider.getDriver(), AlfaBankVacanciesPage.class);
    }

}
