package pages.common;

import driver.DriverProvider;
import org.openqa.selenium.WebDriver;

public class AbstractPage {

    public WebDriver getDriver(){
        return DriverProvider.getDriver();
    }
}
