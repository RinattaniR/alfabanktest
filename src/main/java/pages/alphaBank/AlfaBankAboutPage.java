package pages.alphaBank;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import pages.common.AbstractPage;
import utils.PageUtils;

public class AlfaBankAboutPage extends AbstractPage {

    private WebElement infoTxt(){
        return getDriver().findElement(By.xpath("//div[@class='info']"));
    }

    public String getInfoTxt(){
        PageUtils.waitForPageLoading();
        return PageUtils.getInnerText(infoTxt());
    }
}
