package pages.alphaBank;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import pages.common.AbstractPage;
import utils.PageUtils;

public class AlfaBankVacanciesPage extends AbstractPage {

    private WebElement aboutLnk(){
        return getDriver().findElement(By.xpath("//a[@href='/about/']"));
    }

    public void followAboutLink(){
        PageUtils.waitForPageLoading();
        PageUtils.clickIfEnabled(aboutLnk());
    }
}
