package pages.alphaBank;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import pages.common.AbstractPage;
import utils.PageUtils;

public class AlfaBankStartPage extends AbstractPage {

    private WebElement vacanciesLnk(){
        return getDriver().findElement(By.xpath("//a[contains(@href,'job')]"));
    }

    private WebElement bottomSection(){
        return getDriver().findElement(By.xpath("//div[contains(@class,'sections')]"));
    }


    public void followVacanciesLink(){
        PageUtils.switchToNewWindow();
        PageUtils.waitForDomIsReady();
        PageUtils.fluentWait(bottomSection());
        PageUtils.scrollToElement(bottomSection());
        PageUtils.clickIfEnabled(vacanciesLnk());
    }
}
