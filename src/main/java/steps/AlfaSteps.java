package steps;

import driver.DriverProvider;
import org.jbehave.core.annotations.*;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pages.Pages;
import report.TestReport;

import java.io.IOException;

public class AlfaSteps {

    private static Logger logger = LoggerFactory.getLogger(AlfaSteps.class);
    TestReport report = new TestReport();
 //   Context context = new Context();

    @BeforeScenario(uponType= ScenarioType.ANY)
    public void reloadBrowser() {
        if (DriverProvider.getDriver() != null) {
            WebDriver driver = DriverProvider.getDriver();
            driver.manage().deleteAllCookies();
            driver.navigate().refresh();
        }
    }

    @Given("I get to yandex start page")
    public void getToYandexStartPage(){
        DriverProvider.getDriver().navigate().to(System.getProperty("yandexStartUrl"));
    }

    @Given("I type '$searchString' in searchbox")
    public void typeInSearchBox(String searchString){
        Pages.yandexStartPage().typeInSearchBox(searchString);
    }

    @When("I get to the Alfa bank site")
    public void getToAlfaSite(){
        Pages.yandexSearchResultPage().followAlfaBankLink();
    }

    @When("I get to the vacancies page")
    public void getToVacanciesPage(){
        Pages.alphaBankStartPage().followVacanciesLink();
    }

    @Then("I get to the alpha bank about page")
    public void getToAlphaAboutPage(){
        Pages.alphaBankVacanciesPage().followAboutLink();
    }

    @Then("I generate report")
    public void generateReport() throws IOException {
        String infoTxt = Pages.alphaBankAboutPage().getInfoTxt();
        logger.info("infoTxt = " + infoTxt);
        report.generate(infoTxt);
    }
}
