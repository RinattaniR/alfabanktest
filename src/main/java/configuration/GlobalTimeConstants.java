package configuration;

public class GlobalTimeConstants {

    public static final int LONG_WAIT_SEC = 10;
    public static final int TIMEOUT_SEC = 30;
    public static final int POLLING_WAIT_MILISEC = 100;
}
