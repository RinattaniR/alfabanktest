package launcher;

import driver.DriverProvider;
import embedder.TestEmbedder;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class TestLauncher {

    private static Logger logger = LoggerFactory.getLogger(TestLauncher.class);

    @BeforeClass
    public static void setUp() throws Exception {

        logger.info("========================== 	@BeforeClass_Launcher ================");
        DriverProvider.initDriver();
    }

    @Test
    public void run() {
        TestEmbedder embedder = new TestEmbedder();
        List<String> storyPaths = embedder.storyPaths();
        embedder.runStoriesAsPaths(storyPaths);
    }

   @AfterClass
    public static void tearDown() {

         DriverProvider.getDriver().close();
        logger.info("========================== 	@AfterClass_Launcher  ================");

    }
}
